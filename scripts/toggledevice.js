const {argv} = require('process');
const ewelink_path = argv[2] + '/ewelink-api';
const ewelink = require(ewelink_path);

(async() => {

  const connection = new ewelink({
    email: argv[3],
    password: argv[4],
    region: argv[5],
  });

  const status = await connection.toggleDevice(argv[6], argv[7]);
  console.log(status);

})();
