const {argv} = require('process');
const ewelink_path = argv[2] + '/ewelink-api';
const ewelink = require(ewelink_path);

(async() => {

  const connection = new ewelink({
    email: argv[3],
    password: argv[4],
    region: argv[5],
  });

  const status = await connection.getDevice(argv[6]);
  console.log(status);

})();
