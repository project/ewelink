<?php

namespace Drupal\ewelink\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class EwelinkSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'ewelink.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ewelink_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('email'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('password'),
    ];

    $form['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#default_value' => $config->get('region'),
    ];

    $form['nodejs'] = [
      '#type' => 'details',
      '#title' => t('Nodejs'),
      '#description' => t('Ewelink module requires Node.js'),
      '#open' => TRUE,
    ];

    $form['nodejs']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Node path'),
      '#description' => $this->t('IE: /var/lib/a_directory_containing_node/'),
      '#default_value' => $config->get('path') ?: '/',
    ];

    $form['nodejs']['executable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Executable file'),
      '#description' => $this->t('Leave "node" if unknown'),
      '#default_value' => $config->get('executable') ?: 'node',
    ];

    $form['nodejs']['scripts_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scripts subdirectory'),
      '#description' => $this->t('An ewelink module subdirectory containing nodejs scripts'),
      '#default_value' => $config->get('scripts_path') ?: 'scripts',
    ];

    $form['#prefix'] = "<b>Requirements:</b>
      <ul>
        <li><a href='https://nodejs.org' target=\"_blank\">node.js</a>
        <li>ewelink API [<code>npm install ewelink-api</code>]
      </ul>
      ";

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('email', $form_state->getValue('email'))
      ->set('region', $form_state->getValue('region'))
      ->set('executable', $form_state->getValue('executable'))
      ->set('path', $form_state->getValue('path'))
      ->set('scripts_path', $form_state->getValue('scripts_path'));

    if (!empty($form_state->getValue('password'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('password', $form_state->getValue('password'));
    }

    $this->configFactory->getEditable(static::SETTINGS)
      ->save();

    parent::submitForm($form, $form_state);

  }

}
