<?php

namespace Drupal\ewelink\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for managing eWeLink devices and their interactions.
 *
 * This class handles communication with eWeLink devices, including retrieving
 * device information, toggling device states, and producing device tags.
 */
class EWeLink {

  /**
  * Config settings.
  *
  * @var string
  */
  const SETTINGS = 'ewelink.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Constructs a new Ewelink object.
   */
  public function __construct() {}

  /**
   * Main entry point for the controller.
   *
   * @return array
   *   An empty array.
   */
  public function main() {
    return [];
  }

  /**
   * Executes a Node.js script with the provided options.
   *
   * @param array $options
   *   An array of options for the script.
   *
   * @return array
   *   The result of the script execution.
   */
  private function nodejscall($options) {

    if (empty($options['script'])) {
      return "script is missing";
    }

    $data = [];

    $data['config'] = \Drupal::config(static::SETTINGS);
    $data['module_handler'] = \Drupal::service('module_handler');
    $data['module_path'] = $data['module_handler']->getModule('ewelink')->getPath();
    $data['realpath'] = \Drupal::service('file_system')->realpath($data['module_path']);
    $data['executable'] = $data['config']->get('path') . "/node";
    $data['node_modules_path'] = $data['realpath'] . "/node_modules";

    $data['args']['pieces'] = [
      $data['node_modules_path'],
      $data['config']->get('email'),
      $data['config']->get('password'),
      $data['config']->get('region'),
      $options['deviceid'],
      $options['channel'],
    ];

    $data['args']['string'] = implode(" ", $data['args']['pieces']);

    $data['script'] = $data['realpath'] . "/" . $data['config']->get('scripts_path') . "/" . $options['script'];
    $data['fullcommand'] = $data['executable'] . " " . $data['script'] . " " . $data['args']['string'] . " 2>&1";
    $data['ret'] = exec($data['fullcommand'], $data['out'], $data['err']);
    return $data;

  }

  /**
   * Retrieves the list of eWeLink devices.
   *
   * @return array
   *   A render array for displaying the devices.
   */
  public function devices() {
    $options = ['script' => 'devices.js'];
    $res = $this->nodejscall($options);
    $output = ['#markup' => 'Your ewelink devices'];
    return $output;
  }

  // We mask ewelink device id with custom device id.

  /**
   * This function is a bridge from custom id (tagid) to ewelink id.
   *
   * Converts a custom device tag ID to an eWeLink device ID.
   *
   * @param string $device_tagid
   *   The custom device tag ID.
   *
   * @return array|null
   *   An array containing the device ID and channel, or null if not found.
   */
  private function fromDeviceTagidToDeviceId($device_tagid) {

    $pieces = explode("-", $device_tagid);
    $id = $pieces[0];
    $channel = $pieces[1];

    $devices = $this->staticListOfDevices();
    foreach ($devices as $device) {
      if ($device['tag_id'] == $id) {
        $private_deviceid = [
          'deviceid' => $device['deviceid'],
          'device_tagid' => $id,
          'channel' => $channel,
        ];
        return $private_deviceid;
      }
    }
  }

  /**
   * Retrieves the switch state of a device.
   *
   * @param string $device_tagid
   *   The custom device tag ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the switch state and device ID.
   */
  public function getdeviceswitch($device_tagid) {
    $private_deviceid = $this->fromDeviceTagidToDeviceId($device_tagid);
    $options = [
      'script' => 'getdeviceswitch.js',
      'deviceid' => $private_deviceid['deviceid'],
      'channel' => $private_deviceid['channel'],
    ];
    $res = $this->nodejscall($options);

    $switch = NULL;
    if (!empty($res['out']) && count($res['out'])) {
      foreach ($res['out'] as $row) {
        if (strpos($row, "switch: 'on',")) {
          $switch = "on";
        }
        elseif (strpos($row, "switch: 'off',")) {
          $switch = "off";
        }
      }
    }

    return new JsonResponse([
      'switch' => $switch,
      'id' => $device_tagid,
    ]);
  }

  /**
   * Toggles the state of a device.
   *
   * @param string $device_tagid
   *   The custom device tag ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response indicating success or failure.
   */
  public function toggleDevice($device_tagid) {

    $private_deviceid = $this->fromDeviceTagidToDeviceId($device_tagid);
    $options = [
      'script' => 'toggledevice.js',
      'deviceid' => $private_deviceid['deviceid'],
      'channel' => $private_deviceid['channel'],
    ];

    $res = $this->nodejscall($options);

    if (strpos($res['out'][0], "status: 'ok'")) {

      return new JsonResponse([
        'success' => 1,
        'id' => $device_tagid,
      ]);
    }
    return "";
  }

  /**
   * Produces a tag for a device.
   *
   * @param string $device_tagid
   *   The custom device tag ID.
   * @param string $type
   *   The type of tag to produce.
   * @param array $options
   *   Additional options for the tag.
   *
   * @return array|null
   *   A render array for the tag, or null if the device is not found.
   */
  public function produceTag($device_tagid, $type, $options) {
    $devices = $this->staticListOfDevices();

    foreach ($devices as $device) {
      if ($device['tag_id'] == $device_tagid) {
        return [
          '#theme' => 'ons_switch',
          // ID must be unique.
          '#id' => $device['tag_id'] . "-" . $options['channel'],
          '#label' => $device['label'],
          '#data' => [
            'inching' => $device['inching'],
            'channel' => $options['channel'],
          ],
        ];
      }
    }
  }

  /**
   * Config settings.
   *
   * @inching integer
   *  0 = no inching
   *  1 = inching set
   *  . = more status to come....
   *
   *  @todo implement custom entity to locally manage devices
   *
   * @return array
   *   An array of devices with their configurations.
   */
  private function staticListOfDevices() {

    $devices = [
      "0000000000" => [
        "channel" => [
          1 => "Label for Ch.1",
          2 => "Label for Ch.2",
          3 => "Label for Ch.3",
          4 => "Label for Ch.4",
        ],
        "deviceid" => "0000000000",
        "inching" => 1,
        "label" => "Labol for device",
        "productModel" => "4CHPRO",
        "tag_id" => "ACustomIDUsedAsMask",
      ],
    ];

    return $devices;
  }

}
