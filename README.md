## CONTENTS OF THIS FILE

 * Introduction
 * Design
 * Requirements
 * Installation
 * Related modules
 * Configuration
 * Usage
 * Maintainers


## INTRODUCTION

The module provides

 * Code ato interact with the Ewelink API as described at https://ewelink-api.vercel.app/ 

 * Basic instructions to set up the ecosystem 


## REQUIREMENTS

This module requires 

 * Nodejs (https://nodejs.org)
 
 * Ewelink api (https://ewelink-api.vercel.app)


## INSTALLATION

 * Install the Ewelink module as you would normally install a
   contributed Drupal module. 


## RELATED MODULES

For an easier implementation of your button, controls, etc, the OnsenUI framework is suggested. See https://www.drupal.org/project/onsen 
      
       
## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web Services > Ewelink
    3. Enter your API credentials.
    4. Navigate to Administration > People > Permissions to protect access to the API
    
       
       
## USAGE

 Ewelink for Drupal enable enpoints to interact with the Ewelink API. Implement your own js code to control your Ewelink devices


 
## MAINTAINERS

 * Augusto Fagioli (afagioli) - https://www.drupal.org/u/afagioli
